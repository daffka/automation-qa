/*Добавить возможность забирать из приюта животное с определёнными свойствами («Мама, хочу рыжую собачку!»)*/

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        List<String> dog = new ArrayList<>();
        List<String> cat = new ArrayList<>();
        dog.add("белая");
        cat.add("чёрная");
        Scanner sc = new Scanner(System.in);
        System.out.print("Вы ходите сдать или забрать животное? (нужно ответить сдать/забрать): ");
        String n = sc.nextLine().toLowerCase();
        System.out.print("Какое животное Вы ходите сдать/забрать? (нужно ответить кошка/собака): ");
        String anim = sc.nextLine().toLowerCase();
        System.out.print("Какого цвета животное? (в женском роде): ");
        String color = sc.nextLine().toLowerCase();
        sc.close();
        if (n.equals("сдать")) {
            if (anim.equals("кошка")) {
                cat.add(color);
            } else if (anim.equals("собака")) {
                dog.add(color);
            }
        } else if (n.equals("забрать")) {
            if (anim.equals("кошка")) {
                if (cat.contains(color)) {
                    cat.remove(color);
                    System.out.println("Спасибо, что забрали");
                } else {
                    System.out.println("Извините, у нас нет данного животного");
                }
            }
                else if (anim.equals("собака")) {
                if (dog.contains(color)) {
                    dog.remove(color);
                    System.out.println("Спасибо, что забрали");
                } else {
                    System.out.println("Извините, у нас нет данного животного");
                }
            }
        }
        System.out.println("Имеющиеся собаки: "+dog);
        System.out.println("Имеющиеся кошки: "+cat);

    }
}
