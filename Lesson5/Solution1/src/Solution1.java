/* Создать приют для бездомных кошек и собак. Должна быть реализована возможность добавлять животных в приют, а также
возможность взять из приюта случайное животное */

import java.util.Scanner;

public class Solution1 {
    public static void main(String[] args) {
        // добавляем изначально 10 животных
        for (int i=0; i<10; i++){
            int choice = (int)(Math.random()*10);
            if (choice <= 5) new Cat();
            else new Dog();
        }
        System.out.println(Animal.getShelter());
        Scanner sc = new Scanner(System.in);
        System.out.print("Вы ходите сдать или забрать животное? (нужно ответить сдать/забрать): ");
        String n = sc.nextLine().toLowerCase();
        System.out.print("Какое животное Вы ходите сдать/забрать? (нужно ответить кошка/собака): ");
        String anim = sc.nextLine().toLowerCase();
        sc.close();
        if (n.equals("сдать")) {
            if (anim.equals("кошка")) {
                new Cat();
            } else if (anim.equals("собака")) {
                new Dog();
            }
        } else if (n.equals("забрать")) {
            if (anim.equals("кошка")) {
                Animal.remove(Cat.class);
            } else if (anim.equals("собака")) {
                Animal.remove(Dog.class);
            }
        }
        System.out.println(Animal.getShelter());
    }
}

