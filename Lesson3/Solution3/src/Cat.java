public class Cat extends Animal implements fightable{
    String owner;
    String sex;

    Cat(){
        this.home = true;
        this.health = 50;
        this.damage = 5;
    }

    Cat(String name, String owner, String sex){
        this.name = name;
        this.home = true;
        this.owner = owner;
        this.sex = sex;
        if (sex.equals("кошка")){
            this.health = 50;
            this.damage = 5;
        }
        else {
            this.health = 40;
            this.damage = 7;
        }
    }

    @Override
    public void voice(){
        if (sex.equals("кошка")){
            System.out.println("Мур-мур");
        }
        else {
            System.out.println("Мяяяяяяяяяу");
        }
    }

    @Override
    public void eat(){
        System.out.println("Мой хозяин "+owner+" самый лучший, он меня кормит рыбкой каждый день!");
    }

//    @Override
//    public void fight(Cat x, Cat y){
//        for (;;)
//            if ((x.health <= 0) || (y.health <= 0)) {
//                if ((x.health <= 0) && (y.health <= 0)) {
//                    System.out.println(draw);
//                    break;
//                }
//                if (x.health <= 0) {
//                    System.out.println("Битва закончена! Победитель " + x.name + "!");
//                    break;
//                } else {
//                    System.out.println("Битва закончена! Победитель " + y.name + "!");
//                    break;
//                }
//            } else {
//                x.health = x.health - y.damage;
//                y.health = y.health - x.damage;
//                System.out.println("Здоровье " + x.name + " стало равно: " + x.health);
//                System.out.println("Здоровье " + y.name + " стало равно: " + y.health + "\n");
//            }
//    }
}
