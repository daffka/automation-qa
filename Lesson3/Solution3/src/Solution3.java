/*Задача 3. Теперь все существа (животные и не только) умеют драться между собой. На основании кода из прошлых заданий,
реализуйте механизмы межвидовой борьбы.
(Подсказка. Нужно использовать абстрактные классы и интерфейсы). */

public class Solution3 {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Леопольд", "Владимир", "кот");

        Cat cat2 = new Cat();
        cat2.name = "Маркиза";
        cat2.owner = "Олег";
        cat2.sex = "кошка";

        Dog dog1 = new Dog();
        dog1.setName("Рекс");
        dog1.home = true;

        Dog dog2 = new Dog();
        dog2.home = false;

//        dog1.fight(dog1, cat1);
//        cat1.fight(cat2, dog2);
        System.out.println(dog2.fight(cat1));
    }
}
