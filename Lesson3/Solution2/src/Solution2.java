/*Задача 2. Кошки из прошлого задания не поделили территорию. Реализуйте программу «кошачьи бои». */

public class Solution2 {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Леопольд", "Владимир", "кот");
        cat1.voice();
        cat1.run();
        cat1.eat();

        Cat cat2 = new Cat();
        cat2.name = "Маркиза";
        cat2.owner = "Олег";
        cat2.sex = "кошка";
        cat2.voice();
        cat2.run();
        cat2.eat();

//        cat1.fight(cat1, cat2);
    }
}
