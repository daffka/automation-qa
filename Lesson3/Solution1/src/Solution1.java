/*Задача 1.
Есть 2 кошки и 2 собаки.
 1 кошка и 1 кот обладают именем, хозяином и местом жительства.
 1 собака бездомная, но с гордым именем.
 1 собака домашняя, но без имени.
Все животные могут бегать, есть и издавать некий голос
Реализуйте эту ситуацию с использованием Java и ООП. */

public class Solution1 {
    public static void main (String[] args){
        Cat cat1 = new Cat("Леопольд", "Владимир", "кот");
        cat1.voice();
        cat1.run();
        cat1.eat();

        Cat cat2 = new Cat();
        cat2.name = "Маркиза";
        cat2.owner = "Олег";
        cat2.sex = "кошка";
        cat2.voice();
        cat2.run();
        cat2.eat();

        Dog dog1 = new Dog();
        dog1.setName("Рекс");
        dog1.home = true;
        dog1.voice();
        dog1.run();
        dog1.eat();

        Dog dog2 = new Dog();
        dog2.home = false;
        dog2.voice();
        dog2.run();
        dog2.eat();



    }
}