import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class testAnimal {

    @After
    public void clear(){
        Animal.getShelter().clear();
    }


    @Test
    public void testAddDog(){
        ArrayList<String> test = new ArrayList<>();
        test.add("Dog");
        new Dog();
        Assert.assertEquals(test, Animal.getShelter());
    }

    @Test (expected = AssertionError.class)
    public void testNotAddDog(){
        ArrayList<String> test = new ArrayList<>();
        test.add("Cat");
        new Dog();
        Assert.assertEquals(test, Animal.getShelter());
    }

    @Test
    public void testRemoveCat(){
        ArrayList<String> test = new ArrayList<>();
        new Cat();
        Animal.remove(Cat.class);
        Assert.assertEquals(test, Animal.getShelter());
    }

    @Test (expected = AssertionError.class)
    public void testRemoveNotCat(){
        ArrayList<String> test = new ArrayList<>();
        test.add("Cat");
        new Cat();
        Animal.remove(Cat.class);
        Assert.assertEquals(test, Animal.getShelter());
    }

}
