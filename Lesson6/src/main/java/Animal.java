import java.util.ArrayList;
import java.util.List;

abstract class Animal{
    String name;

    private static List<String> shelter = new ArrayList<>();

    void add() {
        shelter.add(getClass().getName());
        System.out.println("Жаль, что вы сдаёте животное :-(");
    }

    void remove(){
        shelter.remove(getClass().getName());
    }

    static void remove(Class name){
        if (shelter.contains(name.getName())){
            shelter.remove(name.getName());
            System.out.println("Спасибо, что забрали");
        }
        else {
            System.out.println("Извините, у нас нет данного животного");
        }
    }

    static List<String> getShelter() {
        return shelter;
    }
}