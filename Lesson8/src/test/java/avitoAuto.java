/*Используя xpath (css) локаторы найти:
        1. Названия всех мотоциклов
        2. Названия автомобилей, объявления о продаже которых были добавлены сегодня
        3. Первую Audi*(любую марку)*/


import drivers.Driver;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class avitoAuto {
    final static WebDriver driver = Driver.getChromeDriver();
    final static String URL = "https://www.avito.ru/moskva/transport";
    final static String CyclesCSS = "div:nth-child(4) > div > div.recommendations-content div.description a";
    final static String AutoTodayXPATH = "//div[1][@class='recommendations-wrapper']//div[@class='created-date' and contains(translate(text(), 'С', 'с'),'сегодня')]/../../h3";
    static String Brand = "KIA";
    final static String BrandOneXPATH = "//div[1][@class='recommendations-wrapper']//div[@class = 'recommendations-item' and contains(., '"+Brand+"')][1]";
    final static String BrandOnePriceXPATH = "//div[1][@class='recommendations-wrapper']//div[@class = 'recommendations-item' and contains(., '"+Brand+"')][1]//div[@itemprop='price']";

    @BeforeClass
    public static void setup() {
        driver.manage().window().maximize();
    }

    @Before
    public void restart(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(URL);
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }

    @Test
    public void TestAllCycles(){
       List<WebElement> Cycles = driver.findElements(By.cssSelector(CyclesCSS));
    }

    @Test
    public void TestTodayCars(){
        List<WebElement> Cars = driver.findElements(By.xpath(AutoTodayXPATH));
    }

    @Test
    public void Brands(){
        WebElement FirstCar = driver.findElement(By.xpath(BrandOneXPATH));
        WebElement FirstCarPrice = driver.findElement(By.xpath(BrandOnePriceXPATH));
        String price = FirstCarPrice.getAttribute("content");
        Assert.assertSame("150000", price);
    }
}
