/*
Создать 2 своих собственных исключения
 */
public class main {
    public static void main(String[] args) {
        try{
            int result = Factorial.getFactorial(0);
            System.out.println(result);
        }
        catch(PositiveException ex){

            System.out.println(ex.getMessage());
            System.out.println(ex.getNumber());
        }
        try{
            String test = NoText.NoText("testё");
            System.out.println(test);
        }
        catch(TextException ex){
            System.out.println(ex);
        }

    }
}
