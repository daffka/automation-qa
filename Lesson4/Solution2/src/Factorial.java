class Factorial{

    public static int getFactorial(int num) throws PositiveException{

        int result = 1;
        if(num<1) throw new PositiveException("Число не должно быть нулевым", num);

        for(int i=1; i<=num; i++){
            result *= i;
        }
        return result;
    }
}