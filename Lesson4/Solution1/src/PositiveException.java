class PositiveException extends Exception{

    private int number;
    public int getNumber(){return number;}
    public PositiveException(String message, int num){

        super(message);
        number=num;
    }
}
