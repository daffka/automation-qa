import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class Survio {

    final static WebDriver driver = Driver.getChromeDriver();
    final static String URL = "https://www.survio.com/ru/";
    final static String FULL_NAME_ID = "reg_name";
    final static String EMAIL_ID = "reg_email";
    final static String PASSWORD_XPATH = "/html/body/section[1]/div/form/div[3]/input";
    final static String CREATE_REQUEST_XPATH = "/html/body/section[1]/div/form/div[4]/a";
    final static String NEW_QUIZ_XPATH = "/html/body/div[2]/div/ul/li[2]";
    final static String REG_EMAIL_ERROR_ID = "reg_email-error";
    final static String FULL_NAME_TEST = "Тестовый Тест Тестович";
    static int numberTest = 4;
    final static String E_MAIL_TEST = "test"+numberTest+"@test.ru";
    final static String E_MAIL_TEST_NEGATIVE = "test@test.ru";
    final static String PASSWORD_TEST = "12345678";


    @Before
    public void setup() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(URL);
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }

    @Test
    public void TestSignUp(){
        WebElement fullname = driver.findElement(By.name(FULL_NAME_ID));
        fullname.clear();
        fullname.sendKeys(FULL_NAME_TEST);
        WebElement eMail = driver.findElement(By.name(EMAIL_ID));
        eMail.clear();
        eMail.sendKeys(E_MAIL_TEST);
        WebElement password = driver.findElement(By.xpath(PASSWORD_XPATH));
        password.clear();
        password.sendKeys(PASSWORD_TEST);
        WebElement createRequestButton = driver.findElement(By.xpath(CREATE_REQUEST_XPATH));
        createRequestButton.click();
        WebElement newQuiz = driver.findElement(By.xpath(NEW_QUIZ_XPATH));
        Assert.assertTrue(newQuiz.isEnabled());
    }

    @Test
    public void TestDoubleEmail(){
        WebElement fullname = driver.findElement(By.name(FULL_NAME_ID));
        fullname.clear();
        fullname.sendKeys(FULL_NAME_TEST);
        WebElement eMail = driver.findElement(By.name(EMAIL_ID));
        eMail.clear();
        eMail.sendKeys(E_MAIL_TEST_NEGATIVE);
        WebElement password = driver.findElement(By.xpath(PASSWORD_XPATH));
        password.clear();
        password.sendKeys(PASSWORD_TEST);
        WebElement createRequestButton = driver.findElement(By.xpath(CREATE_REQUEST_XPATH));
        createRequestButton.click();
        WebElement ErrorEmail = driver.findElement(By.id(REG_EMAIL_ERROR_ID));
        Assert.assertEquals("Этот адрес электронной почты уже занят другим пользователем.", ErrorEmail.getText());
    }

}