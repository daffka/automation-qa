import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.NikeShoes;
import pages.card;
import pages.productReview;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static drivers.Driver.getChromeDriver;

public class NikeBuyTest {
    private static WebDriver driver;

    @Before
    public void setUp(){
        driver = getChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testNikeNameEqual(){
        NikeShoes nikeShoes = new NikeShoes(driver);
        nikeShoes.open();
        String test = nikeShoes.getRandomProductXpath();
        String name = nikeShoes.productName(test);
        String URL = nikeShoes.getURL(test);
        nikeShoes.openProduct(test);
        productReview productReview = new productReview(driver);
        productReview.open(URL);
        String nameNext = productReview.productName();
        Assert.assertEquals(name, nameNext);
    }

    @Test
    public void testNikePriceEqual(){
        NikeShoes nikeShoes = new NikeShoes(driver);
        nikeShoes.open();
        String test = nikeShoes.getRandomProductXpath();
        String price = nikeShoes.productPrice(test);
        String URL = nikeShoes.getURL(test);
        nikeShoes.openProduct(test);
        productReview productReview = new productReview(driver);
        productReview.open(URL);
        String priceNext = productReview.productPrice();
        Assert.assertEquals(price, priceNext);
    }

    @AfterClass
    public static void quit(){
        driver.quit();
    }

    @Test
    public void testBuyShoes(){
        productReview productReview = new productReview(driver);
        productReview.open();
        productReview.buyShoes();
        card card = new card(driver);
        card.open();
        List<WebElement> test = card.buyShoes();
        Assert.assertEquals(test.size(), 1);
    }
}