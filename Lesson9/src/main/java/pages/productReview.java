package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class productReview {
    private WebDriver driver;

    public String productName(){
        String shoesNameXpath = "//div[@id='RightRail']//h1";
        WebElement ProductName = driver.findElement(By.xpath(shoesNameXpath));
        return ProductName.getText();
    }

    public String productPrice(){
        String shoesPriceXpath = "//div[@id='RightRail']//h1/../..//div[@data-test='product-price']";
        WebElement ProductPrice = driver.findElement(By.xpath(shoesPriceXpath));
        return ProductPrice.getText()
                .replace(' ', ' ')
                .replace('p', 'р')
                .replace('y','у');
    }

    public void open(String URL){
        driver.get(URL);
    }

    public void open(){
        driver.get("https://www.nike.com/ru/t/%D0%BA%D1%80%D0%BE%D1%81%D1%81%D0%BE%D0%B2%D0%BA%D0%B8-tanjun-NwTz9kxD/812654-010");
    }

    public productReview(WebDriver driver){
        this.driver = driver;
    }

    public void buyShoes(){
        String size_shoes = "//div[@name='skuAndSize']/label[3]";
        WebElement SizeShoes = driver.findElement(By.xpath(size_shoes));
        String buy_button = "//form[@id='buyTools']//button[@class='ncss-btn-black fs16-sm ncss-base u-medium pb3-sm prl5-sm pt3-sm css-1hzp36c addToCartBtn']";
        WebElement buyButton = driver.findElement(By.xpath(buy_button));
        SizeShoes.click();
        buyButton.click();
    }

}
