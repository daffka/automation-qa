package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NikeShoes {
    private WebDriver driver;

    public String getRandomProductXpath(){
        String numberString = "//*[@id='exp-gridwall-wrapper']//h1/span";
        WebElement numberShoes = driver.findElement(By.xpath(numberString));
        String numberShoesText = numberShoes.getText();
        int len = numberShoesText.length();
        String newS = numberShoesText.substring(1, len-1);
        int a = 1; // Начальное значение диапазона - "от"
        int b = Integer.parseInt(newS)/10; // Конечное значение диапазона - "до"
        int random_number = a + (int) (Math.random() * b); // Генерация random
        return "//div[@data-item-index='" + random_number + "']";
    }

    public String productName(String shoesXpath){
        String shoesNameXpath = shoesXpath+"//p[1]";
        WebElement ProductName = driver.findElement(By.xpath(shoesNameXpath));
        return ProductName.getText();
    }

    public String productPrice(String shoesXpath){
        String shoesPriceXpath = shoesXpath+"//div[@class='prices']/span[2]";
        WebElement ProductPrice = driver.findElement(By.xpath(shoesPriceXpath));
        return ProductPrice.getText()
                .replace(' ', ' ')
                .replace('p', 'р')
                .replace('y','у');
    }

    public void open(){
//        driver.get("https://store.nike.com/ru/ru_ru/pw/%D0%BC%D1%83%D0%B6%D1%87%D0%B8%D0%BD%D1%8B-%D1%81%D0%BF%D0%BE%D1%80%D1%82%D0%B8%D0%B2%D0%BD%D1%8B%D0%B9-%D1%81%D1%82%D0%B8%D0%BB%D1%8C-%D0%BE%D0%B1%D1%83%D0%B2%D1%8C/7puZoneZoi3?ipp=120");
        driver.get("https://store.nike.com/ru/ru_ru/pw/%D0%BC%D1%83%D0%B6%D1%87%D0%B8%D0%BD%D1%8B-%D0%BE%D0%B1%D1%83%D0%B2%D1%8C/7puZoi3");
    }

    public NikeShoes(WebDriver driver){
        this.driver = driver;
    }


    public void openProduct(String shoesXpath){
        WebElement Product = driver.findElement(By.xpath(shoesXpath+"//div[@class='grid-item-content']"));
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", Product);
        Product.click();
    }

    public String getURL(String shoesXpath){
        WebElement Product = driver.findElement(By.xpath(shoesXpath));
        return Product.getAttribute("data-pdpurl");
    }

}
