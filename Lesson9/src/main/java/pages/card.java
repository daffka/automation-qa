package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class card {
    private WebDriver driver;

    public void open(){
        driver.get("https://secure-store.nike.com/RU/checkout/html/cart.jsp?");
    }

    public card(WebDriver driver){
        this.driver = driver;
    }

    public List<WebElement> buyShoes(){
        String product = "//div[@class='ch4_cartItemOptionsContainer']";
        return driver.findElements(By.xpath(product));
    }

}
