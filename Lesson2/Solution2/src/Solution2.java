/*Задача 2.  У вас есть строка “Рассерженный Родион Романович раскричался: «Рано радовались, размокший ремень рюкзака
разорвался”
Необходимо заменить в данной строке все буквы “р” на символ “*”,  используя цикл. */

public class Solution2 {
    public static void main(String[] args) {
        String text = "Рассерженный Родион Романович раскричался: «Рано радовались, размокший ремень рюкзака\n" +
                "разорвался";//
        StringBuilder newtext = new StringBuilder();
        for (int i = 0; i<text.length(); i++){
            String symbol = text.substring(i, i+1);
            if ((symbol.equals("Р")) || (symbol.equals("р"))) {
               newtext.append("*");
            }
            else {
                newtext.append(text.charAt(i));
            }
        }
        System.out.println(newtext);
    }
}