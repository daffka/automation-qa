/*Задача 3. Создать программу, которая будет проверять попало ли случайно выбранное из отрезка [5;155] целое число в
интервал (25;100) и сообщать результат на экран.
Примеры работы программы:
Число 113 не содержится в интервале (25,100)
Число 72 содержится в интервале (25,100) */

public class Solution3 {
    public static void main(String[] args){
        int start1 = 5;
        int end1 = 155;
        int start2 = 25;
        int end2 = 100;
        int random_number = start1 + (int) (Math.random() * end1);
        if ((random_number > start2) && (random_number < end2)) {
            String result = String.format("Число %d содержится в интервале (%d,%d)", random_number, start2, end2);
            System.out.println(result);
        }
        else {
            String result = String.format("Число %d не содержится в интервале (%d,%d)", random_number, start2, end2);
            System.out.println(result);
        }
    }
}