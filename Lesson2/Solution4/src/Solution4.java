/*Задача 4. У вас есть строка “Я у мамы программист”
Необходимо написать программу, которая приводит каждую уникальную букву к строчному регистру и помещает ее в массив.
Все буквы должны находиться в массиве в алфавитном порядке.
Пример работы программы для строки “Hello world”:
    [‘d’,’e’,’h’,’l’,’o’,’r’,’w’] */

import java.text.Collator;
import java.util.Arrays;
import java.util.Locale;

public class Solution4 {
    public static void main(String[] args) {
        String text = "Я у мамы программист";
        text = text.replaceAll("\\s+", "");
        String[] massiv_char = text.split("");
        Collator col = Collator.getInstance(new Locale("ru", "RU"));
        Arrays.sort(massiv_char, col);
        StringBuilder sorted = new StringBuilder();
        for (String aMassiv_char : massiv_char) {
            sorted.append(aMassiv_char);
        }
        StringBuilder uniq_char = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if (!uniq_char.toString().contains(massiv_char[i])) {
                massiv_char[i] = massiv_char[i].toLowerCase();
            }
            if (!uniq_char.toString().contains(massiv_char[i])) {
                uniq_char.append(massiv_char[i]);
            }
        }
        String[] massiv_uniq_char = uniq_char.toString().split("");
        for (int i = 0; i < uniq_char.length(); i++) {
            if (i == uniq_char.length() - 1) {
                System.out.print("'"+massiv_uniq_char[i] + "']");
            } else if (i == 0) {
                System.out.print("['" + massiv_uniq_char[i] + ", ");
            } else {
                System.out.print("'"+massiv_uniq_char[i] + "', ");
            }
        }
    }
}