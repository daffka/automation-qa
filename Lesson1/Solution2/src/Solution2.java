/*
Задача 2. В переменной  double n хранится число  с ненулевой дробной частью. Создайте программу, округляет число n до
ближайшего целого и выводящую результат на экран.
 */
import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число с двумя знаками после запятой: ");
        int n = (int)(sc.nextDouble()+0.5);
        sc.close();
        System.out.println(n);
    }
}