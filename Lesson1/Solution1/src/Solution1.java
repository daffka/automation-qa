/*
Задача 1. В переменных n и j хранятся два целых числа. Создайте программу, выводящую на экран результат деления n на j
с остатком.
Пример вывода программы (для случая, когда в n хранится 21, а в j  хранится 8):
                    21 / 8 = 2 и 5 в остатке
 */
import java.util.Scanner;

public class Solution1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите делимое: ");
        int n = sc.nextInt();
        System.out.println("Введите частное: ");
        int j = sc.nextInt();
        sc.close();
        int chas = n / j;
        int ostat = n % j;
        System.out.println(n+"/"+j+" = " +chas +" и "+ostat+ " в остатке");
    }
}