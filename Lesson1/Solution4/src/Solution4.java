/*
Задача 4. В переменной n хранится целое двузначное число. Создайте программу, вычисляющую и выводящую на экран сумму
всех цифр числа n.
Например для числа 10, сумма его цифр будет 1+0=1
 */
import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите двузначное число: ");
        int n = sc.nextInt();
        sc.close();
        int des=n%10;
        int edin=(n/10)%10;
        System.out.println("Сумма цифр числа " +n +" равна "+(des+edin));

    }
}
