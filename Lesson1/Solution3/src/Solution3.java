/*
Задача 3.
public class Solution3 {
   public static void main(String[] args) {
       double c = 2.0;
       byte n = 50;
       long a = 200
       int result = (a/n) * c;
   }
}
Исправьте программу, чтобы она компилировалась.
 */
public class Solution3 {
    public static void main(String[] args) {
        double c = 2.0;
        byte n = 50;
        long a = 200;
        int result = (int) ((a/n) * c);
    }
}